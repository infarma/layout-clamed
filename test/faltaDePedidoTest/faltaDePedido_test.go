package faltaDePedidoTest

import (
	"layout-clamed/faltaDePedido"
	"testing"
)

func TestCabecalhoResposta(t *testing.T){
	SequencialArquivo := int32(1)
	IdentificadorTipoArquivo := "FAL"
	CodigoFornecedor := "53406138"
	CodigoCliente := "554864"
	NumeroPedido := int32(13911)
	IdentificadorRejeicaoPedido := "N"
	DescricaoMotivoRejeicaoPedido := ""
	NumeroCGCCliente := "84683481046754"

cabecalho := faltaDePedido.CabecalhoResposta(SequencialArquivo,IdentificadorTipoArquivo,CodigoFornecedor,CodigoCliente,NumeroPedido,IdentificadorRejeicaoPedido,DescricaoMotivoRejeicaoPedido,NumeroCGCCliente)


	if cabecalho != "100001FAL05340613855486400013911N                              84683481046754" {
		t.Error("Cabecalho não é compativel")
	}


}

func TestDetalheResposta(t *testing.T){
	SequencialArquivo := int32(2)
	CodigoProduto := "10286   "
	QuantidadeNaoAtendida := int32(2)
	DescricaoMotivoRejeicao := "Vencimento do Lote Menor que Exigido              "

	detalhe := faltaDePedido.DetalheResposta(SequencialArquivo,CodigoProduto,QuantidadeNaoAtendida,DescricaoMotivoRejeicao)


	if detalhe != "20000210286   00002Vencimento do Lote Menor que Exigido              " {
		t.Error("Detalhe não é compativel")
	}

}

func TestFinalizadorResposta(t *testing.T){
	SequencialArquivo := int32(9)
	NumeroTotalProdutosNaoAtendidos := int32(7)
	QuantidadeTotalProdutosNaoAtendidos := int32(10)

	finalizador := faltaDePedido.FinalizadorResposta(SequencialArquivo,NumeroTotalProdutosNaoAtendidos,QuantidadeTotalProdutosNaoAtendidos)


	if finalizador != "300009000070000010" {
		t.Error("Finalizador não é compativel")
	}
}