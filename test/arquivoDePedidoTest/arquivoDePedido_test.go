package arquivoDePedidoTest

import (
	"layout-clamed/arquivoDePedido"
	"os"
	"testing"
)

func TestComposeStruct(t *testing.T) {

	file, err := os.Open("../fileTest/PED_1605143257.PED")
	if err != nil {
		t.Error("Não abriu o arquivo", err)}

	arquivo,err := arquivoDePedido.GetStruct(file)
	if err != nil {
		t.Error("não abriu o arquivo", err)}

	//REGISTRO CABECALHO
	var registroCabecalho arquivoDePedido.RegistroCabecalho
	registroCabecalho.IdentificadorTipoRegistro = "1"
	registroCabecalho.SequencialArquivo = 1
	registroCabecalho.IdentificadorTipoArquivo = "PED"
	registroCabecalho.CodigoFornecedor = "053406138"
	//->/Pedido com Diferentes Clientes
	registroCabecalho.CodigoCliente = "557674"
	//->Pedido com Diferentes Números de pedido
	registroCabecalho.NumeroPedido = 1261
	registroCabecalho.CodigoPromocao = ""
	registroCabecalho.NumeroCGCCliente = ""

	if registroCabecalho.IdentificadorTipoRegistro != arquivo.RegistroCabecalho.IdentificadorTipoRegistro {
		t.Error("Identificação do Tipo de registro não é compativel")
	}
	if registroCabecalho.SequencialArquivo != arquivo.RegistroCabecalho.SequencialArquivo {
		t.Error("Sequencial do Arquivo não é compatível")
	}
	if registroCabecalho.IdentificadorTipoArquivo != arquivo.RegistroCabecalho.IdentificadorTipoArquivo {
		t.Error("Identificação da Extensão do arquivo não é compatível")
	}
	if registroCabecalho.CodigoFornecedor != arquivo.RegistroCabecalho.CodigoFornecedor {
		t.Error("Código do Fornecedor não é compatível")
	}
	if registroCabecalho.CodigoCliente != arquivo.RegistroCabecalho.CodigoCliente {
		t.Error("Código do Cliente não é compatível")
	}
	if registroCabecalho.NumeroPedido != arquivo.RegistroCabecalho.NumeroPedido {
		t.Error("Número do Pedido não é compatível")
	}
	if registroCabecalho.CodigoPromocao != arquivo.RegistroCabecalho.CodigoPromocao {
		t.Error("Código da Promoção não é compatível")
	}
	if registroCabecalho.NumeroCGCCliente != arquivo.RegistroCabecalho.NumeroCGCCliente {
		t.Error("Número CGC não é compatível")
	}

//REGISTRO DETALHE
		var registroDetalhe arquivoDePedido.RegistroDetalhe
		registroDetalhe.IdentificadorTipoRegistro = "2"
		registroDetalhe.SequencialArquivo = 2
		registroDetalhe.CodigoProduto = "281     "
		registroDetalhe.QuantidadePedida = 2

	if registroDetalhe.IdentificadorTipoRegistro != arquivo.RegistroDetalhe[0].IdentificadorTipoRegistro {
		t.Error("Identificação do Tipo de registro não é compativel")
	}
	if registroDetalhe.SequencialArquivo != arquivo.RegistroDetalhe[0].SequencialArquivo {
		t.Error("Sequencial do Arquivo não é compatível")
	}
	if registroDetalhe.CodigoProduto != arquivo.RegistroDetalhe[0].CodigoProduto {
		t.Error("Código do Produto não é compatível")
	}
	if registroDetalhe.QuantidadePedida != arquivo.RegistroDetalhe[0].QuantidadePedida {
		t.Error("Quantidade Pedida não é compatível")
	}

		//Registro Finalizador
	var registroFinalizador arquivoDePedido.RegistroFinalizador
	registroFinalizador.IdentificadorTipoRegistro = "3"
	registroFinalizador.SequencialArquivo = 32
	registroFinalizador.NumeroTotalProdutosPedidos = 30
	registroFinalizador.QuantidadeTotalProdutosPedidos = 86

	if registroFinalizador.IdentificadorTipoRegistro != arquivo.RegistroFinalizador.IdentificadorTipoRegistro {
		t.Error("Identificação do Tipo de registro não é compativel")
	}
	if registroFinalizador.SequencialArquivo != arquivo.RegistroFinalizador.SequencialArquivo {
		t.Error("Sequencial do arquivo não é compativel")
	}
	if registroFinalizador.NumeroTotalProdutosPedidos != arquivo.RegistroFinalizador.NumeroTotalProdutosPedidos {
		t.Error("Número total dos produtos pedidos não é compativel")
	}
	if registroFinalizador.QuantidadeTotalProdutosPedidos != arquivo.RegistroFinalizador.QuantidadeTotalProdutosPedidos {
		t.Error("Quantidade total de produtos pedidos não é compativel")
	}

}
