package  listaDePrecosTest


import (
	"layout-clamed/listaDePrecos"
	"testing"
)

func TestCabecalhoResposta(t *testing.T){
	SequencialArquivo := int32(1)
	IdentificadorTipoArquivo := "PRE"
	CodigoFornecedor := "53406138"
	UnidadeFederacao := "BA"
	DataGeracaoArquivoPreco := int32(19062019)
	cabecalho := listaDePrecos.CabecalhoResposta(SequencialArquivo ,IdentificadorTipoArquivo ,CodigoFornecedor ,UnidadeFederacao ,DataGeracaoArquivoPreco)

	if cabecalho != "100001PRE053406138BA19062019" {
		t.Error("Cabecalho não é compativel")
	}

}

func TestDetalheResposta(t *testing.T){
	SequencialArquivo := int32(2)
	CodigoProduto := "132"
	PrecoUnitario := float32(28.1)
	PercentualDesconto := float32(70.0)
	PercentualRepasseICMS := float32(0.0)
	PercentualICMS := float32(18.0)
	PercentualReducaoBaseICMS := float32(0.0)
	PrecoBaseCalculoICMSSubstituicaoTributaria := float32(0.0)
	ValorICMSSubstituicaoTributaria := float32(0.0)
	NumeroDiasParaPagamento := "63"
	CodigoEANProduto := "7896004700502"
	ProdutoHPC := "N"
	PercentualDescontoAdicionalPedidoEnviado := float32(0.0)
	ValorICMSSubstituicaoTributariaParaProdutosFarmaciaPopularBrasil := float32(0.0)
	QuantidadeEstoqueDisponivel := int32(48378)

	detalhe := listaDePrecos.DetalheResposta(SequencialArquivo , CodigoProduto , PrecoUnitario, PercentualDesconto, PercentualRepasseICMS, PercentualICMS, PercentualReducaoBaseICMS, PrecoBaseCalculoICMSSubstituicaoTributaria, ValorICMSSubstituicaoTributaria , NumeroDiasParaPagamento, CodigoEANProduto, ProdutoHPC , PercentualDescontoAdicionalPedidoEnviado, ValorICMSSubstituicaoTributariaParaProdutosFarmaciaPopularBrasil , QuantidadeEstoqueDisponivel )


	if detalhe != "200002132     0002810070000000001800000000000000000000063 7896004700502       N000000000000048378" {
		t.Error("Detalhe não é compativel")
	}

}

func TestFinalizadorResposta(t *testing.T){
	SequencialArquivo := int32(2614)
	QuantidadeTotalRegistros := int32(2614)

	finalizador := listaDePrecos.FinalizadorResposta(SequencialArquivo,QuantidadeTotalRegistros)


	if finalizador != "30261402614" {
		t.Error("Finalizador não é compativel")
	}
}