package arquivoDePedido

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
)

type RegistroCabecalho struct {
	IdentificadorTipoRegistro string `json:"IdentificadorTipoRegistro"`
	SequencialArquivo         int32  `json:"SequencialArquivo"`
	IdentificadorTipoArquivo  string `json:"IdentificadorTipoArquivo"`
	CodigoFornecedor          string `json:"CodigoFornecedor"`
	CodigoCliente             string `json:"CodigoCliente"`
	NumeroPedido              int32  `json:"NumeroPedido"` 
	CodigoPromocao            int32  `json:"CodigoPromocao"`
	NumeroCGCCliente          string `json:"NumeroCGCCliente"`
}

func (r *RegistroCabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroCabecalho

	err = posicaoParaValor.ReturnByType(&r.IdentificadorTipoRegistro, "IdentificadorTipoRegistro")
	err = posicaoParaValor.ReturnByType(&r.SequencialArquivo, "SequencialArquivo")
	err = posicaoParaValor.ReturnByType(&r.IdentificadorTipoArquivo, "IdentificadorTipoArquivo")
	err = posicaoParaValor.ReturnByType(&r.CodigoFornecedor, "CodigoFornecedor")
	err = posicaoParaValor.ReturnByType(&r.CodigoCliente, "CodigoCliente")
	err = posicaoParaValor.ReturnByType(&r.NumeroPedido, "NumeroPedido")
		return err


}

var PosicoesRegistroCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorTipoRegistro": {0, 1, 0},
	"SequencialArquivo":         {1, 6, 0},
	"IdentificadorTipoArquivo":  {6, 9, 0},
	"CodigoFornecedor":          {9, 18, 0},
	"CodigoCliente":             {18, 24, 0},
	"NumeroPedido":              {24, 32, 0},
	"CodigoPromocao":            {32, 42, 0},
	"NumeroCGCCliente":          {42, 56, 0},
}
