package arquivoDePedido

import gerador_layouts_posicoes "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroFinalizador struct {
	IdentificadorTipoRegistro      string `json:"IdentificadorTipoRegistro"`
	SequencialArquivo              int32  `json:"SequencialArquivo"`
	NumeroTotalProdutosPedidos     int32  `json:"NumeroTotalProdutosPedidos"`
	QuantidadeTotalProdutosPedidos int32  `json:"QuantidadeTotalProdutosPedidos"`
}

func (r *RegistroFinalizador) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroFinalizador

	err = posicaoParaValor.ReturnByType(&r.IdentificadorTipoRegistro, "IdentificadorTipoRegistro")
	err = posicaoParaValor.ReturnByType(&r.SequencialArquivo, "SequencialArquivo")
	err = posicaoParaValor.ReturnByType(&r.NumeroTotalProdutosPedidos, "NumeroTotalProdutosPedidos")
	err = posicaoParaValor.ReturnByType(&r.QuantidadeTotalProdutosPedidos, "QuantidadeTotalProdutosPedidos")

	return err
}

var PosicoesRegistroFinalizador = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorTipoRegistro":      {0, 1, 0},
	"SequencialArquivo":              {1, 6, 0},
	"NumeroTotalProdutosPedidos":     {6, 11, 0},
	"QuantidadeTotalProdutosPedidos": {11, 18, 0},
}
