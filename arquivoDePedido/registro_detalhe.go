package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroDetalhe struct {
	IdentificadorTipoRegistro string `json:"IdentificadorTipoRegistro"`
	SequencialArquivo         int32  `json:"SequencialArquivo"`
	CodigoProduto             string `json:"CodigoProduto"`
	QuantidadePedida         	int32  `json:"QuantidadePedida"`
}

func (r *RegistroDetalhe) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroDetalhe

	err = posicaoParaValor.ReturnByType(&r.IdentificadorTipoRegistro, "IdentificadorTipoRegistro")
	err = posicaoParaValor.ReturnByType(&r.SequencialArquivo, "SequencialArquivo")
	err = posicaoParaValor.ReturnByType(&r.CodigoProduto, "CodigoProduto")
	err = posicaoParaValor.ReturnByType(&r.QuantidadePedida, "QuantidadePedida")

	return err
}

var PosicoesRegistroDetalhe = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorTipoRegistro": {0, 1, 0},
	"SequencialArquivo":         {1, 6, 0},
	"CodigoProduto":             {6, 14, 0},
	"QuantidadePedida":         {14, 19, 0},
}
