package listaDePrecos

type RegistroCabecalho struct {
	IdentificadorTipoRegistro string `json:"IdentificadorTipoRegistro"`
	SequencialArquivo         int32  `json:"SequencialArquivo"`
	IdentificadorTipoArquivo  string `json:"IdentificadorTipoArquivo"`
	CodigoFornecedor          string `json:"CodigoFornecedor"`
	UnidadeFederacao          string `json:"UnidadeFederacao"`
	DataGeracaoArquivoPreco   int32  `json:"DataGeracaoArquivoPreco"`
}
