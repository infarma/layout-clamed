package listaDePrecos

type RegistroFinalizador struct {
	IdentificadorTipoRegistro string `json:"IdentificadorTipoRegistro"`
	SequencialArquivo         int32  `json:"SequencialArquivo"`
	QuantidadeTotalRegistros  int32  `json:"QuantidadeTotalRegistros"`
}
