package listaDePrecos

import (
	"fmt"
	"strings"
)
//working exemple : https://play.golang.org/p/51xI2wHyimv
// Arquivo de Retorno de Falta de Pedido
type ArquivoDeRespostaPedido struct {
	RegistroCabecalho  RegistroCabecalho  `json:"RegistroCabecalho"`
	RegistroDetalhe 	[]RegistroDetalhe 		`json:"RegistroDetalhe"`
	RegistroFinalizador RegistroFinalizador   `json:"RegistroFinalizador"`
}

// Registro Tipo 1 - Cabecalho
func CabecalhoResposta(SequencialArquivo int32,IdentificadorTipoArquivo string,CodigoFornecedor string,UnidadeFederacao string,DataGeracaoArquivoPreco int32) string {
	cabecalho := fmt.Sprint("1")
	cabecalho += fmt.Sprintf("%05d", SequencialArquivo)
	cabecalho += fmt.Sprintf("%s", IdentificadorTipoArquivo)
	cabecalho += fmt.Sprintf("%09s", CodigoFornecedor)
	cabecalho += fmt.Sprintf("%s", UnidadeFederacao)
	cabecalho += fmt.Sprintf("%d", DataGeracaoArquivoPreco)
	return cabecalho
}

// Registro Tipo 2 - Detalhe
func DetalheResposta(SequencialArquivo int32, CodigoProduto string, PrecoUnitario, PercentualDesconto, PercentualRepasseICMS, PercentualICMS, PercentualReducaoBaseICMS, PrecoBaseCalculoICMSSubstituicaoTributaria, ValorICMSSubstituicaoTributaria float32, NumeroDiasParaPagamento, CodigoEANProduto, ProdutoHPC string, PercentualDescontoAdicionalPedidoEnviado, ValorICMSSubstituicaoTributariaParaProdutosFarmaciaPopularBrasil float32, QuantidadeEstoqueDisponivel int32) string {
	detalhe := fmt.Sprint("2")
	detalhe += fmt.Sprintf("%05d", SequencialArquivo)
	detalhe += fmt.Sprintf("%-8s", CodigoProduto)
	detalhe += fmt.Sprintf("%07s", strings.Replace(fmt.Sprintf("%.2f", PrecoUnitario), ".", "", 1))
	detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", PercentualDesconto), ".", "", 1))
	detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", PercentualRepasseICMS), ".", "", 1))
	detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", PercentualICMS), ".", "", 1))
	detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", PercentualReducaoBaseICMS), ".", "", 1))
	detalhe += fmt.Sprintf("%07s", strings.Replace(fmt.Sprintf("%.2f", PrecoBaseCalculoICMSSubstituicaoTributaria), ".", "", 1))
	detalhe += fmt.Sprintf("%07s", strings.Replace(fmt.Sprintf("%.2f", ValorICMSSubstituicaoTributaria), ".", "", 1))
	detalhe += fmt.Sprintf("%-3s", NumeroDiasParaPagamento)
	detalhe += fmt.Sprintf("%-20s", CodigoEANProduto)
	detalhe += fmt.Sprintf("%s", ProdutoHPC)
	detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", PercentualDescontoAdicionalPedidoEnviado), ".", "", 1))
	detalhe += fmt.Sprintf("%07s", strings.Replace(fmt.Sprintf("%.2f", ValorICMSSubstituicaoTributariaParaProdutosFarmaciaPopularBrasil), ".", "", 1))
	detalhe += fmt.Sprintf("%06d", QuantidadeEstoqueDisponivel)
	return detalhe
}

// Registro Tipo 3 - Finalizador
func FinalizadorResposta(SequencialArquivo,QuantidadeTotalRegistros int32) string {
	finalizador := fmt.Sprint("3")
	finalizador += fmt.Sprintf("%05d",SequencialArquivo)
	finalizador += fmt.Sprintf("%05d",QuantidadeTotalRegistros)
	return finalizador
}