package listaDePrecos

type RegistroDetalhe struct {
	IdentificadorTipoRegistro                                        string  `json:"IdentificadorTipoRegistro"`
	SequencialArquivo                                                int32   `json:"SequencialArquivo"`
	CodigoProduto                                                    string  `json:"CodigoProduto"`
	PrecoUnitario                                                    float32 `json:"PrecoUnitario"`
	PercentualDesconto                                               float32 `json:"PercentualDesconto"`
	PercentualRepasseICMS                                            float32 `json:"PercentualRepasseICMS"`
	PercentualICMS                                                   float32 `json:"PercentualICMS"`
	PercentualReducaoBaseICMS                                        float32 `json:"PercentualReducaoBaseICMS"`
	PrecoBaseCalculoICMSSubstituicaoTributaria                       float32 `json:"PrecoBaseCalculoICMSSubstituicaoTributaria"`
	ValorICMSSubstituicaoTributaria                                  float32 `json:"ValorICMSSubstituicaoTributaria"`
	NumeroDiasParaPagamento                                          string  `json:"NumeroDiasParaPagamento"`
	CodigoEANProduto                                                 string  `json:"CodigoEANProduto"`
	ProdutoHPC                                                       string  `json:"ProdutoHPC"`
	PercentualDescontoAdicionalPedidoEnviado                         float32 `json:"PercentualDescontoAdicionalPedidoEnviado"`
	ValorICMSSubstituicaoTributariaParaProdutosFarmaciaPopularBrasil float32 `json:"ValorICMSSubstituicaoTributariaParaProdutosFarmaciaPopularBrasil"`
	QuantidadeEstoqueDisponivel                                      int32   `json:"QuantidadeEstoqueDisponivel"`
}

