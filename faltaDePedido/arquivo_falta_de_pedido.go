package faltaDePedido

import (
	"fmt"
)
//working example : https://play.golang.org/p/ub4fcKKagbv
// Arquivo de Retorno de Falta de Pedido
type ArquivoDeRespostaPedido struct {
	RegistroCabecalho  RegistroCabecalho  `json:"RegistroCabecalho"`
	RegistroDetalhe 	[]RegistroDetalhe 		`json:"RegistroDetalhe"`
	RegistroFinalizador RegistroFinalizador   `json:"RegistroFinalizador"`
}

// Registro Tipo 1 - Cabecalho
func CabecalhoResposta(SequencialArquivo int32, IdentificadorTipoArquivo string, CodigoFornecedor string, CodigoCliente string, NumeroPedido int32, IdentificadorRejeicaoPedido string, DescricaoMotivoRejeicaoPedido string, NumeroCGCCliente string) string {
	cabecalho := fmt.Sprint("1")
	cabecalho += fmt.Sprintf("%05d", SequencialArquivo)
	cabecalho += fmt.Sprintf("%s", IdentificadorTipoArquivo)
	cabecalho += fmt.Sprintf("%09s", CodigoFornecedor)
	cabecalho += fmt.Sprintf("%s", CodigoCliente)
	cabecalho += fmt.Sprintf("%08d", NumeroPedido)
	cabecalho += fmt.Sprintf("%s", IdentificadorRejeicaoPedido)
	if IdentificadorRejeicaoPedido == "S" {
		//Não tenho certeza se o Motivo vem com, no máximo 30 caracteres
		if len(DescricaoMotivoRejeicaoPedido) <= 30 {
			cabecalho += fmt.Sprintf("%-30s", DescricaoMotivoRejeicaoPedido)
		} else {
			cabecalho += fmt.Sprintf("%-30s", DescricaoMotivoRejeicaoPedido[0:30])
		}
	} else {
		cabecalho += fmt.Sprintf("%30s", "")
	}
	cabecalho += fmt.Sprintf("%s", NumeroCGCCliente)

	return cabecalho
}

// Registro Tipo 2 - Detalhe
func DetalheResposta( SequencialArquivo int32,CodigoProduto string,QuantidadeNaoAtendida int32,DescricaoMotivoRejeicao string) string {
	detalhe := fmt.Sprint("2")
	detalhe += fmt.Sprintf("%05d", SequencialArquivo)
	detalhe += fmt.Sprintf("%-8s", CodigoProduto)
	detalhe += fmt.Sprintf("%05d",QuantidadeNaoAtendida)
	detalhe += fmt.Sprintf("%-50s", DescricaoMotivoRejeicao)

	return detalhe
}

// Registro Tipo 3 - Finalizador
func FinalizadorResposta(SequencialArquivo,NumeroTotalProdutosNaoAtendidos,QuantidadeTotalProdutosNaoAtendidos int32) string {
	finalizador := fmt.Sprint("3")
	finalizador += fmt.Sprintf("%05d",SequencialArquivo)
	finalizador += fmt.Sprintf("%05d",NumeroTotalProdutosNaoAtendidos)
	finalizador += fmt.Sprintf("%07d",QuantidadeTotalProdutosNaoAtendidos)
	return finalizador
}