package faltaDePedido

type RegistroCabecalho struct {
	IdentificadorTipoRegistro     string `json:"IdentificadorTipoRegistro"`
	SequencialArquivo             int32  `json:"SequencialArquivo"`
	IdentificadorTipoArquivo      string `json:"IdentificadorTipoArquivo"`
	CodigoFornecedor              string `json:"CodigoFornecedor"`
	CodigoCliente                 string `json:"CodigoCliente"`
	NumeroPedido                  int32  `json:"NumeroPedido"`
	IdentificadorRejeicaoPedido   string `json:"IdentificadorRejeicaoPedido"`
	DescricaoMotivoRejeicaoPedido string `json:"DescricaoMotivoRejeicaoPedido"`
	NumeroCGCCliente              string `json:"NumeroCGCCliente"`


}



