package faltaDePedido

type RegistroDetalhe struct {
	IdentificadorTipoRegistro string `json:"IdentificadorTipoRegistro"`
	SequencialArquivo         int32  `json:"SequencialArquivo"`
	CodigoProduto             string `json:"CodigoProduto"`
	QuantidadeNaoAtendida     int32  `json:"QuantidadeNaoAtendida"`
	DescricaoMotivoRejeicao   string `json:"DescricaoMotivoRejeicao"`
}
