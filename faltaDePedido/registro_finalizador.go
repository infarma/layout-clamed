package faltaDePedido

type RegistroFinalizador struct {
	IdentificadorTipoRegistro           string `json:"IdentificadorTipoRegistro"`
	SequencialArquivo                   int32  `json:"SequencialArquivo"`
	NumeroTotalProdutosNaoAtendidos     int32  `json:"NumeroTotalProdutosNaoAtendidos"`
	QuantidadeTotalProdutosNaoAtendidos int32  `json:"QuantidadeTotalProdutosNaoAtendidos"`
}
